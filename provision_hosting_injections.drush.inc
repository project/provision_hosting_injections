<?php

/**
 * Implements hook_drush_init().
 */
function provision_hosting_injections_drush_init(){
  provision_hosting_injections_provision_register_autoload();
}

/**
 * Register our directory as a place to find provision classes.
 */
function provision_hosting_injections_provision_register_autoload(){
  static $loaded = FALSE;
  if (!$loaded){
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

/**
 * Expose the service type this extension defines to provision.
 *
 * @return
 *   An array with the service type the key, and the default implementation the value.
 */
function provision_hosting_injections_provision_services(){
  provision_hosting_injections_provision_register_autoload();
  return array('provision_hosting_injections' => NULL);
}

/**
 * Implements hook_provision_apache_vhost_config().
 */
function provision_hosting_injections_provision_apache_vhost_config($uri, $data){
  $injection = str_replace("\r", '', d()->hosting_injections_vhost_apache);
  if (!empty($injection)){
    return _provision_hosting_injections_injection($injection);
  }
  return '';
}

/**
 * Implements hook_provision_drupal_config().
 */
function provision_hosting_injections_provision_drupal_config($uri, $data){
  $injection = str_replace("\r", '', d()->hosting_injections_locals);
  if (!empty($injection)){
    return _provision_hosting_injections_injection($injection);
  }
  return '';
}

/**
 * Format injection string.
 */
function _provision_hosting_injections_injection($data){
  return sprintf("\n\n#### Provided by Aegir hosting_injections feature ####\n\n %s\n\n", $data);
}
